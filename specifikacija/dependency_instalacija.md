# Installing Dependencies

### Instaliranje G++ & Make

#### Debian/Ubuntu
```console
$ sudo apt update
$ sudo apt install build-essential
```

#### Fedora
```console
$ sudo dnf check-update
$ sudo dnf install make automake gcc gcc-c++ kernel-devel
```

Može se verifikovati da li su paketi instalirani uz pomoć naredne dve komande:
```console
$ g++ --version
g++ (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0

$ make --version
GNU Make 4.2.1
Built for x86_64-pc-linux-gnu
```

### Instaliranje ALSA, Mesa & X11

#### Debian/Ubuntu
```console
$ sudo apt update
$ sudo apt install libasound2-dev mesa-common-dev libx11-dev libxrandr-dev libxi-dev xorg-dev libgl1-mesa-dev libglu1-mesa-dev
```

#### Fedora
```console
$ sudo dnf check-update
$ sudo dnf install alsa-lib-devel mesa-libGL-devel libX11-devel libXrandr-devel libXi-devel libXcursor-devel libXinerama-devel
```
