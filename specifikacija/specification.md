## Description of use case `Main Menu` ##

 * __Name__: Main Menu

 * __Actors__: User

 * __Short description__: User starts the application. User choose one of the actions that main menu offers.

 * __Basic flow__:
    1. Player starts application, finishes or quits current Gameplay,
returns from one of following: New Game, Continue, Options.
    2. Application show Main menu, which has following buttons: New Game, Continue, Options, Quit.
    3. User clicks on one of the buttons:
        * 3.1 If New Game is clicked, then New Game use case precides.
        * 3.2 If Continue is clicked, then Continue use case precides.
        * 3.3 If Options is clicked, then Options use case precides.
        * 3.4 If Quit is clicked, then Application terminates.

* __Alternative flow__:
    1. If Player forcefully quits Application, Application just terminates.

* __Preconditions__: Application is closed, or in one of the
following states: Gameplay, New Game, Load Game, Settings.

* __Postconditions__: /

* __Sequence Diagram__: 

![Main Menu Sequence Diagram](sequence_diagram_mainmenu.png) 


## Description of use case `Continue` ##

**Name**: `Continue`

**Short description**: User chooses whether he wants to continue one of the previously unfinished games.

**Actors**: User

**Precondition**: Application is running and one of the previous games is unfinished.

**Postconditions**: /

**Event Flow**:

1. User selects button `Continue` from the main menu.

2. Application lists all saved games.
  - 2.1. If the list is empty, player goes back to the main menu.
  - 2.2. User selects wanted game and goes to the step 3. 

3. Application reads file which corresponds to the selected game.
  - 3.1. If the reading fails, player goes to main menu

4. Application starts the game from where the user left off.

**Alternative flow**: /

**Subflow**: /

**Special condition**: /

**Additional information**: /

**Sequence diagram**:

![Sequence diagram Continue](sequence_diagram_continue.png)


## Description of use case `New game` ##

**Name**: `New game`

**Short description**: User, from the main menu, selects 'New game', prompting a new window. This window provides both the access to the configuration of the next game, as well as the option to start it.

**Actors**: User

**Precondition**: Application is in the 'Main menu' state.

**Postconditions**: /

**Event Flow**:

1. User selects the 'New game' button.

2. The 'New game' window appears. From this window:

    * 2.1 User selects total number of players from a drop down menu.
        * 2.1.1 Changes immediately reflect on the number of inputs for the step 2.2.
        * 2.1.2 If the number of players has been increased, disable 2.5 (starting the game) again.

    * 2.2 User inputs the names of the players who will participate in the game into the appropriate input fields.
        * 2.2.1 If all the names have been inputted correctly, enable step 2.5 (starting the game).
        * 2.2.2 If the names have not been inputted correctly, alert the User.

    * 2.3 User chooses the difficulty of the game from a drop down menu.
    * 2.4 User sets the timer duration for the next game by dragging the horizontal scroll bar.
    * 2.5 If the User selects the 'Start game' button, proceed to 'Gameplay'. Disabled at the start.
    * 2.6 If the User selects the 'Back' button, close the 'New game' window, returning them to the main menu.

**Sequence diagram**:

![Sequence diagram New game](sequence_diagram_newgame.png)


## Description of use case `Options` ##

**Name**: `Options`

**Short description**: User selects Options from Main Menu or Pause, new window opens where he can change game options.

**Actors**: User

**Precondition**: App is in Main Menu state or App is in Pause state.

**Postconditions**: Game options are saved.

**Event Flow**:

1. User selects Options button
2. App shows Options window
3. If user selects Music toggle button, music starts or stops based on previous state:
    - 3.1. If toggle was ON now is OFF and music stops
    - 3.2. If toggle was OFF now is ON and music starts
4. If user selects Sound toggle button, sound effects are enabled or disabled based on previous state:
    - 4.1. If toggle was ON now is OFF and sound effects are disabled
    - 4.2. If toggle was OFF now is ON and sound effects are enabled
5. If user selects Back button he is returned to previous state

**Sequence diagram**:

![Sequence diagram Options](sequence_diagram_options.png)

## Description of use case `Gameplay` ##

**Name**: `Gameplay`

**Short description**: User starts a game from the `New game` or `Continue` submenu. The application, respectively, starts a new game with the previously defined settings, or loads a previously started game; then begins the render loop until the criteria for its end are met.

**Actors**: User

**Precondition**: Application is in the 'New game' or 'Continue' state.

**Postconditions**: /

**Event Flow**:

1. User selects the 'Play' button from the 'New game' or 'Continue' submenu.
    * 1.1 If from 'New game', the application fetches the game parameters as previously set by the user.
    * 1.2 If from 'Continue', the application fetches the program state and flags step 3 as redundant.
2. Application creates the game instance.
3. Several game critical resources are instanced or generated, including the map and its layout, as well as players and initial hexes.
4. The game window is returned to the player.
5. Until the game is interrupted or the number of rounds reaches a threshold, the following repeats:
    * 5.1 The game window is refreshed.
    * 5.2 A pool of randomly chosen hexes is generated.
    * 5.3 A timer (re)starts for the current player.
        * 5.3.1 If the timer reaches 0, the player's turn ends, skipping to step 5.8;
    * 5.4 A player chooses a hex from their pool via left click, then hovers over an empty hex location on the grid, and attempts to place it via another left click.
        * 5.4.1 If the position is valid for the selected hex, it is placed, the map is updated, the hex is removed from the pool and the player's turn concludes.
            * 5.4.1.2 After placing a hex, adjacency boni and penalties are resolved, and area modifiers are exerted (if applicable) and fetched.
        * 5.4.2 If the position is not valid, the user is warned.
        * 5.4.3 Optionally, the player may choose to rotate the selected tile by right-clicking it.
    * 5.5 Optionally, the player may move the camera's position by using the WASD keys.
    * 5.6 Optionally, the player may choose to select a tile from the map, having its pertinent details displayed in a separate sub-window.
    * 5.7 Optionally, the player may choose to pause the game by pressing 'P' or 'Esc', halting the timer and gaining access to several game options.
    * 5.8 The 'end of turn' bonuses are added to the respective player's score.
    * 5.9 Game designates the next player as 'Current player'.
6. Once a certain number of turns has passed or if the current player chooses the 'Save and exit' option from the 'Pause' menu, the game ends.
    * 6.1 If the turn limit was reached, the player with the highest score is declared to be the winner. The game's result is saved, and can be accessed from main menu, to which the application returns.
    * 6.2 If 'Save and exit' was chosen, the application's state is saved to be fetched when 'Continue' is selected. The application returns to the main menu.
**Sequence diagram**:

![Sequence diagram Gameplay](sequence_diagram_gameplay.png)

# Class diagram

![class diagram](class_diagram.png)

# Components Diagram

![components diagram](component_diagram.png)

# Concept preview

![Concept preview](concept_preview.png)

# Implemented class diagram

![class diagram](impl_class_diag.jpg)