#include "../include/game.hpp"

#include <iostream>

Game::Game()
{
}

void Game::endTurn()
{
    currentPlayer = (currentPlayer % numOfPlayers + 1);
    if (currentPlayer == 1)
        currentRound++;
}

void Game::setNextPlayer()
{
    currentPlayer = (currentPlayer + 1) % numOfPlayers;
}

bool Game::updateTimer()
{
    timerVal -= GetFrameTime();
    if (timerVal <= 0)
    {
        resetTimer();
        return true;
    }
    return false;
}
