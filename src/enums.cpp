#include "enums.hpp"

std::string hexIDstring[] = {"Cabin", "Castle", "Dock", "Farm", "House", "Market", "Mill", "Mine", "Sheep", "Smelter", "Tower", "Village", "Water", "Grass",
                             "Forest", "Hill", "River", "RiverV", "RiverL", "RiverS", "Mountain", "Rocks", "Island", "WaterR", "Stonehill", "Lumber", "Watemill",
                             "EMPTY", "NONE"};

std::string getToolTipFromID(hexID h)
{
    switch (h)
    {
    case (hexID::CABIN):
        return "Cabins must be placed next to a mountain or a hill.\nThey have +3 from adjacent sheep hexes.";
    case (hexID::DOCK):
        return "Docks must be placed land-side towards an adjacent building. They gain a\none-time +4 for every adjacent open water hex (not rivers) when you place them.";
    case (hexID::HOUSE):
        return "Estates have -1 for every adjacent hex. Once they are completely\nsurrounded by hexes, they have +8 instead.";
    case (hexID::MARKET):
        return "Markets gain a one time +1 for every adjacent building that belongs to you,\nand a one-time + however much an adjacent building is worth if it belongs to an opponent.";
    case (hexID::VILLAGE):
        return "Villages have +1 for every adjacent town building (cabins, docks, estates, markets)\nand +2 for other adjacent villages, but they will be set to\n-2 points for the entire game if an opponent's hex is adjacent to them.";
    case (hexID::TOWER):
        return "Towers gain a one time -3, and an additional -3 for each adjacent hex belonging\nto an opponent. Afterwards, it gains +3 when an opponent places a hex next to it.";
    case (hexID::FARM):
        return "Farms have +2 from every adjacent river, and +4 from every adjacent watermill.";
    case (hexID::MILL):
        return "After placing a mill, give all adjacent farms +3 if they are next to a river or a watermill.";
    case (hexID::MINE):
        return "Mines must be placed next to a mountain or a hill, and gain +2 from\nbeing adjacent to them. If a new mine is built next to an existing one,\nit steals all the points from the older one.";
    case (hexID::SHEEP):
        return "Sheep grant +3 to adjacent cabins, and gain +2 for every adjacent pasture.";
    case (hexID::SMELTER):
        return "Smithies will grant +2 to future production buildings\n(mines, lumberyards, mills, farms, sheep, other smithies) placed next to it.";
    case (hexID::LUMBER):
        return "Lumberyards gain a one-time +2 for each adjacent forest. If any belong to an opponent,\nthey lose -2, and the lumberyard gains an addition +2.";
    case (hexID::WATERMILL):
        return "Watermills give +4 to adjacent farms.";
    case (hexID::RIVER):
    case (hexID::RIVERL):
    case (hexID::RIVERV):
    case (hexID::RIVERSTART):
        return "Rivers give +2 adjacent to farms, and have +2 for each end that flows into another hex.";
    case (hexID::FOREST):
        return "Forests gain +1 for every adjacent forest.";
    case (hexID::MOUNTAIN):
    case (hexID::HILL):
    case (hexID::ROCKS):
    case (hexID::STONEHILL):
        return "Mountains and hills gain +1 for every other adjacent mountain or hill.";
    case (hexID::GRASS):
        return "Pastures provide +2 to adjacent sheep.";
    case (hexID::WATER):
    case (hexID::ISLAND):
    case (hexID::WATERROCKS):
        return "Water and island tiles are worth 2 points.";
    default:
        throw "Invalid hexID passed to tooltip generator.";
    }
    throw "Critical tooltip generator error!";
};