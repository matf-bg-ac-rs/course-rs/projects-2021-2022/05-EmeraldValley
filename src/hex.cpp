#include "../include/hex.hpp"
#include "app.hpp"
#include <string>
#include "map.hpp"

///////////// HEX //////////////////
Hex::Hex(int q, int r, hexID hid)
{
    m_q = q;
    m_r = r;
    m_s = 0 - q - r;
    id = hid;
    model = LoadModel(getPathFromID(hid).c_str());
}

Hex::~Hex()
{
    UnloadModel(model);
}

///////////// HEXBASIC //////////////////

HexBasic::HexBasic(int q, int r, hexID type, float rotation, unsigned int owner) : Hex(q, r, type)
{
    m_owner = owner;
    m_rotation = rotation;
    m_rotation_progress = rotation;
    m_q_progress = (float)q;
    m_r_progress = (float)r;
}

void HexBasic::setCoords(int q, int r)
{
    m_q = q;
    m_r = r;
    m_s = 0 - q - r;
}

void HexBasic::setRotationProgress(float rot)
{
    m_rotation_progress = rot;
}

void HexBasic::setRotation(int rot)
{
    rot = rot % 360;
    if (rot < 0)
        rot += 360;
    m_rotation = rot;
}

void HexBasic::updatePositionProgress()
{
    Vector2 currLen = Vector2Subtract({(float)m_q, (float)m_r}, {m_q_progress, m_r_progress});
    Vector2 direction = Vector2Normalize(currLen);
    float new_q_progress = m_q_progress + direction.x * GetFrameTime() * 10.0;
    float new_r_progress = m_r_progress + direction.y * GetFrameTime() * 10.0;
    Vector2 movement = Vector2Subtract({(float)m_q, (float)m_r}, {new_q_progress, new_r_progress});
    if (Vector2DotProduct(currLen, movement) < 0)
    {
        m_q_progress = m_q;
        m_r_progress = m_r;
    }
    else
    {
        m_q_progress = new_q_progress;
        m_r_progress = new_r_progress;
    }
}

void HexBasic::incrementRotation()
{
    m_rotation += 60;
    int rotFloor = floor(m_rotation);
    float m_rotation_dec = m_rotation - rotFloor;

    rotFloor = rotFloor % 360;
    m_rotation = rotFloor + m_rotation_dec;
    if (m_rotation < 0)
        m_rotation += 360;
}

////////////// HEXMODULAR /////////////

HexModular::HexModular(int q, int r, hexID type, float rotation, unsigned int owner) : HexBasic(q, r, type, rotation, owner)
{
    edges = generateEdgesFromType(type);
}

///////////// MISC //////////////////
bool operator==(const Hex &a, const Hex &b)
{
    return a.m_q == b.m_q && a.m_r == b.m_r;
}

bool operator!=(const Hex &a, const Hex &b)
{
    return !(a == b);
}

Model *Hex::getModel()
{
    return &model;
}

std::ostream & operator<<(std::ostream &out, HexBasic &h){
    std::pair<int, int> qr = h.getCoords();
    out << "(" << qr.first << ", " << qr.second << ", " << 0-qr.first-qr.second << ")" <<
    " owned by " << h.getOwner() << ". It's rotation angle was " << h.getRotation() << ".";
    return out;
}

std::string getPathFromID(hexID id){
    std::string path = "resources/models/";
    switch(id){
        case hexID::CABIN: {path += "building_cabin.obj"; break;}
        case hexID::CASTLE: {path += "building_castle.obj"; break;}
        case hexID::DOCK: {path += "building_dock.obj"; break;}
        case hexID::FARM: {path += "building_farm.obj"; break;}
        case hexID::HOUSE: {path += "building_house.obj"; break;}
        case hexID::MARKET: {path += "building_market.obj"; break;}
        case hexID::MILL: {path += "building_mill.obj"; break;}
        case hexID::MINE: {path += "building_mine.obj"; break;}
        case hexID::SHEEP: {path += "building_sheep.obj"; break;}
        case hexID::SMELTER: {path += "building_smelter.obj"; break;}
        case hexID::TOWER: {path += "building_tower.obj"; break;}
        case hexID::VILLAGE: {path += "building_village.obj"; break;}
        case hexID::WATERMILL: {path += "building_water.obj"; break;}
        case hexID::GRASS: {path += "grass.obj"; break;}
        case hexID::FOREST: {path += "grass_forest.obj"; break;}
        case hexID::HILL: {path += "grass_hill.obj"; break;}
        case hexID::RIVER: {path += "river_straight.obj"; break;}
        case hexID::RIVERV: {path += "river_cornerSharp.obj"; break;}
        case hexID::RIVERL: {path += "river_corner.obj"; break;}
        case hexID::RIVERSTART: {path += "river_start.obj"; break;}
        // case hexID::SAND: {path += "sand.obj"; break;}
        case hexID::MOUNTAIN: {path += "stone_mountain.obj"; break;}
        case hexID::ROCKS: {path += "stone_rocks.obj"; break;}
        case hexID::ISLAND: {path += "water_island.obj"; break;}
        case hexID::WATERROCKS: {path += "water_rocks.obj"; break;}
        case hexID::STONEHILL: {path += "stone_hill.obj"; break;}
        case hexID::LUMBER: {path += "dirt_lumber.obj"; break;}
        case hexID::WATER: {path += "water.obj"; break;}
        case hexID::EMPTY: {path += "white.obj"; break;}
        default: throw "Wrong ID passed to path generator!";
    }
    return path.c_str();
}

hexID getIDfromImagePath(std::string s){
    s=s.substr(17, 3);
         if(s=="cbn") return hexID::CABIN;
    else if(s=="cst") return hexID::CASTLE;
    else if(s=="dok") return hexID::DOCK;
    else if(s=="frm") return hexID::FARM;
    else if(s=="hos") return hexID::HOUSE;
    else if(s=="mrk") return hexID::MARKET;
    else if(s=="mil") return hexID::MILL;
    else if(s=="min") return hexID::MINE;
    else if(s=="shp") return hexID::SHEEP;
    else if(s=="smt") return hexID::SMELTER;
    else if(s=="twr") return hexID::TOWER;
    else if(s=="vlg") return hexID::VILLAGE;
    else if(s=="wtr") return hexID::WATER;
    else if(s=="grs") return hexID::GRASS;
    else if(s=="frs") return hexID::FOREST;
    else if(s=="hil") return hexID::HILL;
    else if(s=="rvr") return hexID::RIVER;
    else if(s=="rvv") return hexID::RIVERV;
    else if(s=="rvl") return hexID::RIVERL;
    else if(s=="rvs") return hexID::RIVERSTART;
    // else if(s=="snd") return hexID::SAND;
    else if(s=="mtn") return hexID::MOUNTAIN;
    else if(s=="rck") return hexID::ROCKS;
    else if(s=="isl") return hexID::ISLAND;
    else if(s=="wrr") return hexID::WATERROCKS;
    else if(s=="sth") return hexID::STONEHILL;
    else if(s=="lmb") return hexID::LUMBER;
    else if(s=="wtm") return hexID::WATERMILL;
    else throw "Invalid string passed to hexID generator!";

}

cat getCategoryFromID(hexID id){
    switch (id)
    {
    case hexID::CABIN: { return cat::TOWN_BUILDING;}
    case hexID::CASTLE: { return cat::FORTIFICATION;}
    case hexID::DOCK: { return cat::TOWN_BUILDING;}
    case hexID::FARM: { return cat::INDUSTRY;}
    case hexID::HOUSE: { return cat::TOWN_BUILDING;}
    case hexID::MARKET: { return cat::TOWN_BUILDING;}
    case hexID::MILL: { return cat::INDUSTRY;}
    case hexID::MINE: { return cat::INDUSTRY;}
    case hexID::SHEEP: { return cat::INDUSTRY;}
    case hexID::SMELTER: { return cat::INDUSTRY;}
    case hexID::TOWER: { return cat::FORTIFICATION;}
    case hexID::VILLAGE: { return cat::TOWN_BUILDING;}
    case hexID::WATER: { return cat::SEA;}
    case hexID::GRASS: { return cat::LAND;}
    case hexID::FOREST: { return cat::FOREST;}
    case hexID::HILL: { return cat::ELEVATION;}
    case hexID::RIVER: { return cat::RIVER;}
    case hexID::RIVERV: { return cat::RIVER;}
    case hexID::RIVERL: { return cat::RIVER;}
    case hexID::RIVERSTART: { return cat::RIVER;}
    // case hexID::SAND: { return cat::LAND;}
    case hexID::MOUNTAIN: { return cat::ELEVATION;}
    case hexID::ROCKS: { return cat::ELEVATION;}
    case hexID::ISLAND: { return cat::SEA;}
    case hexID::WATERROCKS: { return cat::SEA;}
    case hexID::STONEHILL: { return cat::ELEVATION;}
    case hexID::LUMBER: { return cat::INDUSTRY;}
    case hexID::WATERMILL: { return cat::WATERMILL;}
    default: { throw "Critical error in category fetching!";}
    }
}

std::vector<hexID> generateEdgesFromType(hexID id){
    switch(id){
    case hexID::DOCK: return {hexID::DOCK, hexID::DOCK, hexID::DOCK, hexID::WATER, hexID::WATER, hexID::WATER};
    case hexID::RIVERSTART: return {hexID::GRASS, hexID::GRASS, hexID::GRASS, hexID::GRASS, hexID::RIVER, hexID::GRASS};
    case hexID::RIVER: return {hexID::GRASS, hexID::RIVER, hexID::GRASS, hexID::GRASS, hexID::RIVER, hexID::GRASS};
    case hexID::RIVERV: return {hexID::GRASS, hexID::GRASS, hexID::GRASS, hexID::GRASS, hexID::RIVER, hexID::RIVER};
    case hexID::RIVERL: return {hexID::RIVER, hexID::GRASS, hexID::GRASS, hexID::GRASS, hexID::RIVER, hexID::GRASS};
    case hexID::WATERMILL: return {hexID::GRASS, hexID::RIVER, hexID::GRASS, hexID::GRASS, hexID::RIVER, hexID::GRASS};
    
    default:
        throw "Critical error in edge type generation!\n";
    }
}

bool getModularFromID(hexID id){
    switch(id){
    case hexID::CABIN: return false;
    case hexID::CASTLE: return false;
    case hexID::DOCK: return true;
    case hexID::FARM: return false;
    case hexID::HOUSE: return false;
    case hexID::MARKET: return false;
    case hexID::MILL: return false;
    case hexID::MINE: return false;
    case hexID::SHEEP: return false;
    case hexID::SMELTER: return false;
    case hexID::TOWER: return false;
    case hexID::VILLAGE: return false;
    case hexID::WATER: return false;
    case hexID::GRASS: return false;
    case hexID::FOREST: return false;
    case hexID::HILL: return false;
    case hexID::RIVER: return true;
    case hexID::RIVERV: return true;
    case hexID::RIVERL: return true;
    case hexID::RIVERSTART: return true;
    // case hexID::SAND: return false;
    case hexID::MOUNTAIN: return false;
    case hexID::ROCKS: return false;
    case hexID::ISLAND: return false;
    case hexID::WATERROCKS: return false;
    case hexID::STONEHILL: return false;
    case hexID::LUMBER: return false;
    case hexID::WATERMILL: return true;
    default: throw "Wrong ID passed to class checker!";

    }
}