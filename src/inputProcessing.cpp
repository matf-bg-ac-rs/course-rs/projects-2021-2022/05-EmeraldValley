#include "app.hpp"
#include "geometry.hpp"
#include "hex.hpp"
#include "sounds.hpp"
#include <vector>

//TODO make separate hex creation functions outside of the input loop
void App::processInput(Map &hexMap, Camera &camera, cameraController *cCtrl, hexID hID, int *currentPlayer, soundPlayer *sounds, std::vector<int> *scores)
{
    //Find the 3d coordinates of the point on the xz plane that the mouse interescts
    Vector3 rayIntersectionPoint = getRayIntersectionPoint(camera);

    //Use those 3d coordinates to determine the hexagon coordinates
    std::pair<int, int> hoveredHex = spaceToHex(rayIntersectionPoint, hexSpacing);

    //If no hex has been passed from Pool (at the start or after placing a hex) and there isn't a hex currently being placed
    if (hID != hexID::NONE && !(this->pickedHex))
    {
        float randRot = (rand() % 6) * 60.0;
        if (getModularFromID(hID))
            pickedHex = new HexModular(hoveredHex.first, hoveredHex.second, hID, randRot, *currentPlayer);
        else
            pickedHex = new HexBasic(hoveredHex.first, hoveredHex.second, hID, randRot, *currentPlayer);
        applyShader(*pickedHex);
    }
    else if (this->pickedHex && this->pickedHex->getID() == hID)
    {
        if (pickedHex->getCoords() != hoveredHex)
        {
            pickedHex->setCoords(hoveredHex.first, hoveredHex.second);
            if ((hexMap.Find(hoveredHex) && hexMap.Find(hoveredHex)->isVacant()))
                placeable = hexMap.checkPlaceable(pickedHex, mapRadius);
            else
                placeable = false;
        }
    }
    else if (this->pickedHex)
    {
        delete pickedHex;
        pickedHex = nullptr;
    }

    if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && !mouseInPool() && hexMap.Find(hoveredHex))
    {
        if ((*hexMap.getMap())[hoveredHex]->isVacant() == false)
        {
            sounds->play(CLICK);
            if (!App::selected || hoveredHex != App::selectedHex)
            {
                App::selected = true;
                App::selectedHex = hoveredHex;
                App::selectedHexID = (*hexMap.getMap())[hoveredHex]->getID();
            }
            else
                App::selected = !App::selected;
        }
        else if (hID != hexID::NONE && !showOptions)
        {
            if (placeable)
            {
                pickedHex->setOwner(*currentPlayer);
                HexBasic *toMap = pickedHex;
                hexMap.getMap()->erase(hoveredHex);
                hexMap.Insert(hoveredHex, toMap);
                shouldClear = true;
                sounds->play(PLACE);
                App::selected = false;
                shouldEndTurn = true;
                this->setLastClickedId(hexID::NONE);
                pickedHex = nullptr;
                std::vector<HexBasic *> adjHexes;
                int q = hoveredHex.first;
                int r = hoveredHex.second;
                hexMap.getAdjacentHexes(q, r, &adjHexes);
                for (int i = 0; i < 6; i++)
                {
                    std::pair<int, int> p = hexMap.hexDirections[i];
                    if (!adjHexes[i])
                    {
                        Hex *newEmpty = new Hex(q + p.first, r + p.second, hexID::EMPTY);
                        std::pair<int, int> newCoords = {q + p.first, r + p.second};
                        hexMap.Insert(newCoords, newEmpty);
                    }
                    else if (!adjHexes[i]->isVacant())
                        hexMap.applyBonuses(toMap, adjHexes[i], i, scores);
                }
            }
        }
        else
            selected = false;
    }
    else if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && !mouseInPool())
        App::selected = false;
    else if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && !mouseInPool() && !hexMap.Find(hoveredHex))
    {
        if (this->pickedHex)
        {
            this->setLastClickedId(hexID::NONE);
            delete pickedHex;
            pickedHex = nullptr;
            sounds->play(ERROR);
        }
    }
    else if (IsMouseButtonPressed(MOUSE_BUTTON_RIGHT) && !mouseInPool() && pickedHex)
    {
        if (pickedHex->getRotation() == pickedHex->getRotationProgress())
        {
            pickedHex->incrementRotation();
            sounds->play(ROTATE);
        }
        if ((hexMap.Find(hoveredHex) && hexMap.Find(hoveredHex)->isVacant()))
            placeable = hexMap.checkPlaceable(pickedHex, mapRadius);
    }

    if (IsKeyPressed(KEY_ESCAPE))
        showOptions = !showOptions;
    if (IsKeyPressed(KEY_SPACE))
        shouldEndTurn = true;
    if (IsKeyPressed(KEY_H))
        showHotkeys = !showHotkeys;
    if (IsKeyPressed(KEY_J))
        showHexScores = !showHexScores;
    if (IsKeyPressed(KEY_K))
        showPlayerColors = !showPlayerColors;
    if (IsKeyPressed(KEY_O))
        animateHexMovement = !animateHexMovement;

    //CAMERA CONTROLS
    if (IsKeyDown(KEY_W) || IsKeyDown(KEY_S) || IsKeyDown(KEY_A) || IsKeyDown(KEY_D))
        cCtrl->WASDpressed();

    float wheel = GetMouseWheelMove();
    if (wheel != 0)
        cCtrl->mWheelMoved(wheel);

    if (IsMouseButtonDown(MOUSE_BUTTON_MIDDLE))
    {
        cCtrl->MMBpressed();
    }
    UpdateCamera(&camera);
}

bool App::mouseInPool()
{
    if (GetMouseX() >= 528 && GetMouseX() <= 1074 && GetMouseY() >= 763 && GetMouseY() <= 867)
        return true;
    else
        return false;
}