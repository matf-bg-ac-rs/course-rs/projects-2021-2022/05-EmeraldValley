#include "../include/newGameSetup.hpp"


Setup::Setup(){}


void Setup::drawSetup(){

    Menu m;

    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(ImColor(85, 85, 75))); 
    ImGui::SetNextWindowPos(ImVec2(GetScreenWidth()/2 - 380/2, GetScreenHeight()/2-300));
    ImGui::SetNextWindowSize(ImVec2(400, 250));
    ImGui::SetNextWindowBgAlpha(0.35f);
    ImGui::Begin("Setup", 0, window_flags);
        
        //PLAYER
        const char* items[] = { "2 Players", "3 Players", "4 Players"};
       // static int item_current_idx = 0; // Here we store our selection data as an index.
        const char* combo_preview_value = items[m_numOfPlayers];  // Pass in the preview value visible before opening the combo (it could be anything)
        ImGui::Spacing();
        
        if (ImGui::BeginCombo("", combo_preview_value))
        {
            

            for (int n = 0; n < IM_ARRAYSIZE(items); n++)
            {
                const bool is_selected = (m_numOfPlayers == n);
                if (ImGui::Selectable(items[n], is_selected)){
                    m_numOfPlayers = n;
                }
                // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
                if (is_selected)
                    ImGui::SetItemDefaultFocus();
            }
            ImGui::EndCombo();
        }

        //TIMER
        ImGui::Spacing();
        ImGui::Text("Seconds per turn : %d second", m_timerSetup);
        ImGui::Spacing();
        
        static int selected = -1;
        
        for (int n = 0; n < 3; n++)
        {
            char buf[32];
            sprintf(buf, "%d ", m_timerOptions[n]);
            if (ImGui::Selectable(buf, selected == n)){
                selected = m_timerOptions[n];
                m_timerSetup = m_timerOptions[n];
            }
        }

        ImGui::Spacing();
        ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.138f, 0.5f, 0.5f, 0.65));
        ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.138f, 0.7f, 0.5f, 0.65));
        ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0.138f, 0.9f, 0.5f, 0.65));
        if(ImGui::Button("Start!", ImVec2(100, 25))){
            setSetupFlag(false);
            m.setMainMenuFlag(false);

            // KEY PART

            isGameReady = true;


        }
        ImGui::SameLine();
        if(ImGui::Button("Back", ImVec2(100, 25))){
            setSetupFlag(false);
            m.setInMainMenuFlag(true);
        }
        ImGui::PopStyleColor(3);
    ImGui::PopStyleColor();

    ImGui::End();
}

int Setup::getNumPlayers(){
    return m_numOfPlayers + 2;
}

int Setup::getSetupTimer(){
    return m_timerSetup;
}
