#include "../include/sounds.hpp"

soundPlayer::soundPlayer()
{
    hexSelect = LoadSound("resources/sounds/hexSelectedFromPool.mp3");
    hexPlaced = LoadSound("resources/sounds/hexPlaced.mp3");
    hexRotate = LoadSound("resources/sounds/hexRotate.mp3");
    hexError = LoadSound("resources/sounds/errorHexPlace.mp3");
    uiClick = LoadSound("resources/sounds/uiClick.mp3");
    exitPrompt = LoadSound("resources/sounds/exitPrompt.mp3");
    victory = LoadSound("resources/sounds/victory.mp3");
    draw = LoadSound("resources/sounds/draw.mp3");
    timerExpired = LoadSound("resources/sounds/timerExpired.mp3");
};

soundPlayer::~soundPlayer()
{
    UnloadSound(hexSelect);
    UnloadSound(hexPlaced);
    UnloadSound(hexRotate);
    UnloadSound(hexError);
    UnloadSound(uiClick);
    UnloadSound(exitPrompt);
    UnloadSound(victory);
    UnloadSound(draw);
    UnloadSound(timerExpired);
}

void soundPlayer::play(SoundID s)
{
    switch (s)
    {
    case (SELECT):
    {
        PlaySound(hexSelect);
        break;
    }
    case (PLACE):
    {
        PlaySound(hexPlaced);
        break;
    }
    case (ROTATE):
    {
        PlaySound(hexRotate);
        break;
    }
    case (ERROR):
    {
        PlaySound(hexError);
        break;
    }
    case (CLICK):
    {
        PlaySound(uiClick);
        break;
    }
    case (PROMPT):
    {
        PlaySound(exitPrompt);
        break;
    }
    case (VICTORY):
    {
        PlaySound(victory);
        break;
    }
    case (DRAW):
    {
        PlaySound(draw);
        break;
    }
    case (TIMER):
    {
        PlaySound(timerExpired);
        break;
    }
    default:
        throw "Error catching SoundID!";
    }
};