#include "cameraController.hpp"

cameraController::~cameraController(){};
// MOVEMENT
void cameraController::WASDpressed()
{
    Vector3 cameraFront = Vector3Subtract(m_camera->target, m_camera->position);
    cameraFront.y = 0;
    cameraFront = Vector3Normalize(cameraFront);
    cameraFront = Vector3Scale(cameraFront, 0.1);

    if (IsKeyDown(KEY_W))
    {
        m_camera->position = Vector3Add(m_camera->position, cameraFront);
        m_camera->target = Vector3Add(m_camera->target, cameraFront);
    }
    if (IsKeyDown(KEY_S))
    {
        Vector3 cameraBack;
        cameraBack.x = cameraFront.x * (-1.0);
        cameraBack.y = 0.0;
        cameraBack.z = cameraFront.z * (-1.0);
        m_camera->position = Vector3Add(m_camera->position, cameraBack);
        m_camera->target = Vector3Add(m_camera->target, cameraBack);
    }
    if (IsKeyDown(KEY_A))
    {
        Vector3 cameraLeft;
        cameraLeft.x = cameraFront.z;
        cameraLeft.y = 0.0;
        cameraLeft.z = cameraFront.x * -1.0;
        m_camera->position = Vector3Add(m_camera->position, cameraLeft);
        m_camera->target = Vector3Add(m_camera->target, cameraLeft);
    }
    if (IsKeyDown(KEY_D))
    {
        Vector3 cameraRight;
        cameraRight.x = cameraFront.z * -1.0;
        cameraRight.y = 0.0f;
        cameraRight.z = cameraFront.x;
        m_camera->position = Vector3Add(m_camera->position, cameraRight);
        m_camera->target = Vector3Add(m_camera->target, cameraRight);
    }
}

// ZOOM
void cameraController::mWheelMoved(float wheel)
{
    Vector3 cameraFront = Vector3Subtract(m_camera->target, m_camera->position);
    if (wheel == 1 && m_camera->position.y > 5.0)
        m_camera->position =
            Vector3Add(m_camera->position, Vector3Normalize(cameraFront));

    if (wheel == -1 && m_camera->position.y < 20.0)
        m_camera->position =
            Vector3Subtract(m_camera->position, Vector3Normalize(cameraFront));
}
// ROTATION
void cameraController::MMBpressed()
{
    if (IsMouseButtonDown(MOUSE_BUTTON_MIDDLE))
    {
        Vector2 mouseDelta = GetMouseDelta();

        float s = sin(mouseDelta.x / 80);
        float c = cos(mouseDelta.x / 80);

        Vector3 planarTarget = m_camera->target;
        planarTarget.y = m_camera->position.y;

        Vector3 planarDiff = Vector3Subtract(m_camera->position, planarTarget);

        float xNew = planarDiff.x * c - planarDiff.z * s;
        float zNew = planarDiff.x * s + planarDiff.z * c;

        Vector3 newVec = {xNew, 0.0f, zNew};

        m_camera->position = Vector3Add(planarTarget, newVec);
    }
}