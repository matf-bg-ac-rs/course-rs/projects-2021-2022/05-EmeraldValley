#include "../include/map.hpp"
#include "../include/hex.hpp"
#include "../include/app.hpp"
#include <utility>

#include <iostream>

Map::Map()
{
}

Map::~Map()
{
    for (auto &h : m_hexMap)
        delete h.second;
}

Hex *Map::Find(std::pair<int, int> clickedHex)
{
    auto it = m_hexMap.find(clickedHex);

    if (it != m_hexMap.end())
    {
        Hex *hex = it->second;
        return hex;
    }
    return nullptr;
}

void Map::Insert(std::pair<int, int> &coord, Hex *h)
{
    m_hexMap.insert({coord, h});
}

hexMap *Map::getMap()
{
    return &m_hexMap;
}

std::vector<HexBasic *> Map::getAdjacentHexes(int q, int r, std::vector<HexBasic *> *adjHexVec)
{
    int i = 0;
    for (auto p : hexDirections)
    {
        i++;
        adjHexVec->push_back((HexBasic *)this->Find({q + p.first, r + p.second}));
    }
    return *adjHexVec;
}

bool Map::checkPlaceable(HexBasic *h, int radius)
{
    std::vector<HexBasic *> adjHexVec;

    int q = h->getCoords().first;
    int r = h->getCoords().second;

    if (abs(q) > radius || abs(r) > radius || abs(q + r) > radius)
        return false;

    getAdjacentHexes(q, r, &adjHexVec);

    //TODO clean up hanging variables

    hexID id = h->getID();
    bool modular = getModularFromID(id);

    bool hasOwnAdjacent = false;
    bool conditionMet = !(id == hexID::CABIN || id == hexID::DOCK || id == hexID::MINE || id == hexID::LUMBER);
    bool restrictionBroken = false;

    // bool conditionMet = true;
    // bool restrictionBroken = false;

    int i = 0;
    for (HexBasic *adjHex : adjHexVec)
    {
        if (adjHex && !adjHex->isVacant())
        {
            if (h->getOwner() == adjHex->getOwner())
                hasOwnAdjacent = true;
            checkEdges(h, adjHex, &conditionMet, &restrictionBroken, i);
        }
        i++;
        if (restrictionBroken)
            break;
    }
    //TODO create tooltips instead of writing messages to std::out
    if (hasOwnAdjacent && conditionMet && !restrictionBroken)
        return true;
    else
    {
        //writePlacementMessage(id);
        return false;
    }
}

void Map::checkEdges(HexBasic *originHex, HexBasic *adjHex, bool *conditionMet, bool *restrictionBroken, int i)
{
    hexID originEdge = originHex->getEdgeID(i);
    hexID adjEdge = adjHex->getEdgeID((i + 3) % 6);
    cat adjCat = getCategoryFromID(adjEdge);
    switch (originEdge)
    {
    case hexID::CABIN:
    {
        if (adjEdge == hexID::RIVER)
            *restrictionBroken = true;
        if (adjCat == cat::ELEVATION)
            *conditionMet = true;
        break;
    }
    case hexID::DOCK:
    {
        if (adjEdge == hexID::RIVER)
            *restrictionBroken = true;
        if (adjCat == cat::TOWN_BUILDING || adjCat == cat::INDUSTRY || adjCat == cat::WATERMILL)
            *conditionMet = true;
        break;
    }
    case hexID::MINE:
    {
        if (adjEdge == hexID::RIVER)
            *restrictionBroken = true;
        if (adjCat == cat::ELEVATION)
            *conditionMet = true;
        break;
    }
    case hexID::LUMBER:
    {
        if (adjEdge == hexID::RIVER)
            *restrictionBroken = true;
        if (adjCat == cat::FOREST)
            *conditionMet = true;
        break;
    }
    case hexID::RIVER:
    {
        if (adjCat != cat::RIVER && adjCat != cat::WATERMILL && adjCat != cat::SEA)
            *restrictionBroken = true;
        break;
    }
    case hexID::RIVERV:
    {
        if (adjCat != cat::RIVER && adjCat != cat::WATERMILL && adjCat != cat::SEA)
            *restrictionBroken = true;
        break;
    }
    case hexID::RIVERL:
    {
        if (adjCat != cat::RIVER && adjCat != cat::WATERMILL && adjCat != cat::SEA)
            *restrictionBroken = true;
        break;
    }
    case hexID::RIVERSTART:
    {
        if (adjCat != cat::RIVER && adjCat != cat::WATERMILL && adjCat != cat::SEA)
            *restrictionBroken = true;
        break;
    }
    case hexID::WATER:
    {
        break;
    }
    case hexID::ISLAND:
    {
        break;
    }
    case hexID::WATERROCKS:
    {
        break;
    }
    default:
    {
        if (adjCat == cat::RIVER)
            *restrictionBroken = true;
    }
    }
}

void writePlacementMessage(hexID id)
{
    std::cout << "Incorrect placement!" << std::endl;
}

//--------------------------------------
int Map::generateMap(int numOfPlayers, bool real)
{
    int mapRadius = 5;
    if (numOfPlayers == 2)
    {
    }
    else if (numOfPlayers == 3)
        mapRadius = 6;
    else if (numOfPlayers == 4)
        mapRadius = 7;
    else
        mapRadius = 8;

    if (real)
    {
        int player = 1;
        for (int i = 0; i < 6; i++)
        {
            if ((i == 0 || i == 2 || i == 3 || i == 5) && numOfPlayers == 2)
                continue;
            if ((i == 1 || i == 3 || i == 5) && numOfPlayers == 3)
                continue;
            if ((i == 1 || i == 4) && numOfPlayers == 4)
                continue;

            float factor = 0.5;
            int q = mapRadius * (hexDirections[i].first) * factor;
            int r = mapRadius * (hexDirections[i].second) * (factor);

            if (q > 0)
                q = floor(q);
            else
                q = ceil(q);

            if (r > 0)
                r = floor(r);
            else
                r = ceil(r);

            if (numOfPlayers == 4)
            {
                if (i == 0)
                    r++;
                if (i == 2)
                {
                    q++;
                    r--;
                }
                if (i == 3)
                    r--;
                if (i == 5)
                {
                    r++;
                    q--;
                }
            }
            HexBasic *h = new HexBasic(q, r, hexID::CASTLE, 0.0, player);
            m_hexMap.insert({{q, r}, h});

            std::vector<HexBasic *> adjHexes;
            getAdjacentHexes(q, r, &adjHexes);
            for (int j = 0; j < 6; j++)
            {
                std::pair<int, int> p = hexDirections[j];
                if (!adjHexes[j])
                {
                    std::pair<int, int> newCoords = {q + p.first, r + p.second};
                    Hex *newEmpty = new Hex(newCoords.first, newCoords.second, hexID::EMPTY);
                    m_hexMap.insert({newCoords, newEmpty});
                }
            }
            player++;
        }
    }
    else
    {
        srand(time(NULL));
        int radius = mapRadius + 1;
        int q = -1 * radius;
        int r = 0;
        int rdm = 2;

        for (int l = 0; l < 5; l++)
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < radius; j++)
                {
                    int rng = rand() % rdm;
                    if (rng < 2)
                    {
                        std::vector<HexBasic *> adjHexVec;
                        getAdjacentHexes(q, r, &adjHexVec);
                        bool setTile = false;
                        for (HexBasic *adjHex : adjHexVec)
                        {
                            if ((adjHex != nullptr && !adjHex->isVacant()) || m_hexMap.size() == 0)
                                setTile = true;
                        }

                        if (setTile)
                        {
                            rng = rand() % 40;
                            std::pair<int, int> newCoords = {q, r};
                            HexBasic *h;
                            if (rng < 3)
                                h = new HexBasic(q, r, hexID::WATERROCKS, 0.0);
                            else if (rng < 6)
                                h = new HexBasic(q, r, hexID::ISLAND, 0.0);
                            else if (rng < 7)
                                h = new HexBasic(q, r, hexID::MOUNTAIN, 0.0);
                            else if (rng < 8)
                                h = new HexBasic(q, r, hexID::STONEHILL, 0.0);
                            else if (rng < 9)
                                h = new HexBasic(q, r, hexID::ROCKS, 0.0);
                            else
                                h = new HexBasic(q, r, hexID::WATER, 0.0);
                            m_hexMap.insert({newCoords, h});
                        }
                    }
                    q += hexDirections[i].first;
                    r += hexDirections[i].second;
                }
            }
            rdm++;
            ;
            radius++;
            q = -1 * radius;
            r = 0;
        }

        radius = mapRadius;

        for (int l = 1; l <= mapRadius; l++)
        {
            q = l * -1;
            r = 0;
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < l; j++)
                {
                    Hex *h = new Hex(q, r, hexID::EMPTY);
                    std::pair<int, int> newCoords = {q, r};
                    m_hexMap.insert({newCoords, h});
                    q += hexDirections[i].first;
                    r += hexDirections[i].second;
                }
            }
        }
        Hex *h = new Hex(0, 0, hexID::EMPTY);
        m_hexMap.insert({{0, 0}, h});
    }
    return mapRadius;
}

void Map::applyBonuses(HexBasic *origin, HexBasic *adj, int i, std::vector<int> *scores)
{
    hexID originEdge = origin->getEdgeID(i);
    hexID originMainID = origin->getID();
    hexID originID = origin->getID();
    hexID adjEdge = adj->getEdgeID((i + 3) % 6);
    hexID adjID = adj->getID();
    cat originCat = getCategoryFromID(originEdge);
    cat adjCat = getCategoryFromID(adjEdge);
    int owner1 = origin->getOwner();
    int owner2 = adj->getOwner();
    int oldScore1 = origin->getScore();
    int oldScore2 = adj->getScore();

    if (owner1 != owner2)
    {
        if (adjEdge == hexID::VILLAGE)
        {
            int vilPenalty = oldScore2 + 2;
            adj->modifyScore(-vilPenalty);
        }
        else if (adjEdge == hexID::TOWER)
        {
            adj->modifyScore(3);
        }
    }
    if (adjEdge == hexID::HOUSE)
    {
        if (oldScore2 > -5)
            adj->modifyScore(-1);
        else
            adj->modifyScore(13);
    }
    switch (originCat)
    {
    case (cat::TOWN_BUILDING):
    {
        if (adjEdge == hexID::VILLAGE && oldScore2 > -2)
        {
            adj->modifyScore(1);
        }
        switch (originID)
        {
        case (hexID::CABIN):
        {
            if (adjEdge == hexID::SHEEP)
                origin->modifyScore(3);
            break;
        }
        case (hexID::DOCK):
        {
            break;
        }
        case (hexID::HOUSE):
        {
            if (oldScore1 == -5)
                origin->modifyScore(13);
            else
                origin->modifyScore(-1);
            break;
        }
        case (hexID::MARKET):
        {
            if (adjCat == cat::TOWN_BUILDING || adjCat == cat::INDUSTRY ||
                adjCat == cat::FORTIFICATION || adjCat == cat::WATERMILL)
            {
                if (owner1 == owner2)
                    origin->modifyScore(1);
                else
                    origin->modifyScore(oldScore2);
            }
            break;
        }
        case (hexID::VILLAGE):
        {
            if (owner1 != owner2)
            {
                int vilPenalty = oldScore2 + 2;
                adj->modifyScore(-vilPenalty);
                break;
            }
            if (adjCat == cat::TOWN_BUILDING)
                origin->modifyScore(1);
            if (adjEdge == hexID::VILLAGE)
            {
                origin->modifyScore(1);
                adj->modifyScore(1);
            }
            break;
        }
        default:
            throw "Invalid hexID passed during score calculation!";
        }
        break;
    }
    case (cat::FORTIFICATION):
    {
        if (oldScore1 == 0)
            origin->modifyScore(-3);
        if (owner1 != owner2)
            origin->modifyScore(-3);
        break;
    }
    case (cat::INDUSTRY):
    {
        if (adjEdge == hexID::SMELTER)
        {
            origin->modifyScore(2);
            break;
        }
        switch (originID)
        {
        case (hexID::FARM):
        {
            if (getCategoryFromID(adjID) == cat::RIVER)
                origin->modifyScore(2);
            else if (adjID == hexID::WATERMILL)
                origin->modifyScore(4);
            break;
        }
        case (hexID::MILL):
        {
            if (adjEdge == hexID::FARM && oldScore2 > 0)
                adj->modifyScore(3);
            break;
        }
        case (hexID::MINE):
        {
            if (adjEdge == hexID::MINE)
            {
                origin->modifyScore(oldScore2);
                adj->modifyScore(-oldScore2);
            }
            else if (adjCat == cat::ELEVATION)
            {
                origin->modifyScore(2);
            }
            break;
        }
        case (hexID::SHEEP):
        {
            if (adjEdge == hexID::CABIN)
                adj->modifyScore(3);
            else if (adjEdge == hexID::GRASS)
                origin->modifyScore(2);
            break;
        }
        case (hexID::SMELTER):
        {
            break;
        }
        case (hexID::LUMBER):
        {
            if (adjEdge == hexID::FOREST)
            {
                if (owner1 == owner2)
                    origin->modifyScore(2);
                else
                {
                    origin->modifyScore(4);
                    adj->modifyScore(2);
                }
            }
            break;
        }
        default:
            throw "Invalid hexID passed during score calculation!";
        }
        break;
    }
    case (cat::SEA):
    {
        if (oldScore1 == 0)
            origin->modifyScore(2);
        if (adjEdge == hexID::RIVER)
            adj->modifyScore(2);
        break;
    }
    case (cat::LAND):
    {
        if (adjEdge == hexID::SHEEP)
            adj->modifyScore(2);
        break;
    }
    case (cat::ELEVATION):
    {
        if (adjCat == cat::ELEVATION)
        {
            origin->modifyScore(1);
            adj->modifyScore(1);
        }
        else if (adjEdge == hexID::MINE)
            adj->modifyScore(2);
        break;
    }
    case (cat::FOREST):
    {
        if (adjCat == cat::FOREST)
        {
            origin->modifyScore(1);
            adj->modifyScore(1);
        }
        break;
    }
    case (cat::WATERMILL):
    {
        if (adjEdge == hexID::FARM)
            adj->modifyScore(4);
        break;
    }
    case (cat::RIVER):
    {
        if (originEdge == hexID::RIVER)
        {
            origin->modifyScore(2);
            if (adjEdge == hexID::RIVER)
                adj->modifyScore(2);
        }
        break;
    }
    default:
        throw "Critical error during score calculation!";
    }
    if (getCategoryFromID(originID) == cat::RIVER)
        if (adjEdge == hexID::FARM)
            adj->modifyScore(2);
    if (originID == hexID::DOCK)
    {
        if (adjCat == cat::SEA)
            origin->modifyScore(4);
    }

    int newScore1 = origin->getScore();
    int newScore2 = adj->getScore();

    (*scores)[owner1 - 1] += newScore1 - oldScore1;
    (*scores)[owner2 - 1] += newScore2 - oldScore2;
}
