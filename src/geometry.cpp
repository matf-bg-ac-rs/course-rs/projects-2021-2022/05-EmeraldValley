#include <raylib-cpp.hpp>
#include <raymath.h>
#include <cmath>
#include <limits>
#include <utility>
#include <iostream>

Vector3 getRayIntersectionPoint(Camera camera)
{
	Ray mouseray = GetMouseRay(GetMousePosition(), camera);
	Vector3 orig = mouseray.position;
	Vector3 dir = mouseray.direction;
	Vector3 planeOrig = {0.0f, 0.4f, 0.0f};
	Vector3 planeNormal = {0.0f, 1.0f, 0.0f};
	float intersectionDistance;
	float d = Vector3DotProduct(dir, planeNormal);
	float Epsilon = std::numeric_limits<float>::epsilon();

	if (std::abs(d) > Epsilon) // if dir and planeNormal are not perpendicular
	{
		float const tmp_intersectionDistance = Vector3DotProduct(Vector3Subtract(planeOrig, orig), planeNormal) / d;
		if (tmp_intersectionDistance > 0.0)
		{ // allow only intersections
			intersectionDistance = tmp_intersectionDistance;
		}
	}
	else
		return {0, 0, 0};

	Vector3 rayIntersectionPoint = Vector3Add(mouseray.position, (Vector3Scale(mouseray.direction, intersectionDistance)));
	return rayIntersectionPoint;
}

std::pair<int, int> spaceToHex(Vector3 position, float hexSpacing)
{
	float q = (sqrt(3.0) * position.x - position.z) / (2 * sqrt(3) * hexSpacing);
	float r = position.z / (sqrt(3.0) * hexSpacing);
	float s = 0 - q - r;

	int qR = round(q);
	int rR = round(r);
	int sR = round(s);

	float q_diff = abs(qR - q);
	float r_diff = abs(rR - r);
	float s_diff = abs(sR - s);

	if ((q_diff > r_diff) && (q_diff > s_diff))
		qR = -rR - sR;
	else if (r_diff > s_diff)
		rR = -qR - sR;
	else
		sR = -qR - rR;
	return std::make_pair(qR, rR);
}