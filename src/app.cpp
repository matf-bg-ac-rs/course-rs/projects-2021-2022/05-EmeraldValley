#include "../include/app.hpp"
#include "../include/raylib/rlights.hpp"
#include "../include/raylib/raymath.h"
#include "../include/hex.hpp"
#include "../include/geometry.hpp"
#include "../include/menu.hpp"
#include "../include/hex_info.hpp"
#include "../include/trigger.hpp"
#include "../include/game.hpp"
#include "../include/cameraController.hpp"
#include "../include/sounds.hpp"

#include <map>
#include <string>
#include <vector>

#include "../include/imgui/imgui.h"
#include "../include/imgui/imgui_impl_opengl3.h"
#include "../include/imgui/imgui_impl_raylib.h"

#define FPS 144
int framesCounter = 0;

void App::run()
{
    init();
    InitAudioDevice();
    raylib::Window w(1600, 900, "Emerald Valley");
    Image icon = LoadImage("resources/icon.png");
    SetWindowIcon(icon);
    UnloadImage(icon);

    Camera camera = makeCamera();
    cameraController cCtrl(&camera);
    shader = makeShader();

    Model tower = LoadModel("resources/models/building_house.obj");
    Vector3 towerPos = {0.0f, -2.0f, 0.0f};

    for (int i = 0; i < tower.materialCount; i++)
        tower.materials[i].shader = shader;

    m_music = LoadMusicStream("resources/music/country.mp3");
    SetMusicVolume(m_music, 0.4);
    PlayMusicStream(m_music);
    Trigger pool;
    Menu m;
    Options o;
    Game game;
    Map map;
    Map decorMap;
    soundPlayer sounds;
    hexMap *hM = map.getMap();
    SetTargetFPS(FPS);
    std::vector<int> playerScores;

    ImGui::CreateContext();
    ImGui_ImplRaylib_Init();
    ImGui::StyleColorsDark();
    ImGui_ImplOpenGL3_Init();

    o.m_playPauseMusic = true;
    while (!gameShouldClose)
    {
        float cameraPos[3] = {camera.position.x, camera.position.y,
                              camera.position.z};
        SetShaderValue(shader, shader.locs[SHADER_LOC_VECTOR_VIEW], cameraPos,
                       SHADER_UNIFORM_VEC3);

        UpdateMusicStream(m_music);

        if (o.m_playPauseMusic)
            PauseMusicStream(App::m_music);
        else
            ResumeMusicStream(App::m_music);

        initImgui(this->screenWidth, this->screenHeight);
        ClearBackground(CLITERAL(Color){180, 150, 140, 255});
        BeginDrawing();
        if (m.getMainMenuFlag())
        {
            gameShouldClose = m.getShouldClose();
            BeginMode3D(camera);
            DrawModel(tower, towerPos, 2.5f, WHITE);
            EndMode3D();

            m.initMainMenu();
            if (m.IsGameReady())
            {

                if (m.timer != 0)
                    game.setTimerVal(m.timer);

                game.setNumOfPlayers(m.numOfPlayers);
                mapRadius = map.generateMap(m.numOfPlayers, true);
                decorMap.generateMap(m.numOfPlayers, false);
                camera.position = {0.0, 9.0, 9.0};
                for (int i = 0; i < 2; i++)
                {
                    hexMap *hM;
                    if (i == 0)
                        hM = map.getMap();
                    else
                        hM = decorMap.getMap();
                    for (hexMap::iterator i = hM->begin(); i != hM->end(); i++)
                    {
                        Hex *h = i->second;
                        if (!h->isVacant())
                            applyShader(*h);
                    }
                }
                for (int i = 0; i < m.numOfPlayers; i++)
                    playerScores.push_back(0);
            }
        }
        else
        {

            int *currentPlayer = game.getCurrentPlayer();
            if (shouldEndTurn)
                game.endTurn();
            shouldEndTurn = false;
            processInput(map, camera, &cCtrl, this->getLastClickedId(), currentPlayer,
                         &sounds, &playerScores);

            BeginMode3D(camera);
            DrawHexes(decorMap.getMap(), true, currentPlayer, &camera);
            DrawHexes(map.getMap(), false, currentPlayer, &camera);

            if (this->pickedHex && !mouseInPool())
                DrawHex(this->pickedHex, false, currentPlayer, &camera);
            EndMode3D();

            if (showHexScores)
            {
                for (auto h : *hM)
                {
                    float posx = h.second->getCoords().first;
                    float posz = h.second->getCoords().second;
                    float x = hexSpacing * (2.0 * posx + posz);
                    float z = hexSpacing * (sqrt(3) * posz);
                    Vector2 hexPosition = GetWorldToScreen((Vector3){x, 1.2f, z}, camera);
                    if (!h.second->isVacant())
                    {
                        HexBasic *hTemp = (HexBasic *)h.second;
                        std::string s = std::to_string(hTemp->getScore());
                        DrawText(s.c_str(),
                                 (int)hexPosition.x - MeasureText(s.c_str(), 20) / 2,
                                 (int)hexPosition.y, 20, WHITE);
                    }
                }
            }
            if (shouldEndTurn ||
                (!showOptions && m.timer != 0 && game.updateTimer()))
            {
                if (game.getTimerVal() == m.timer)
                    sounds.play(ERROR);
                game.resetTimer();
                shouldEndTurn = true;
                shouldClear = true;
                if (pickedHex)
                {
                    setLastClickedId(hexID::NONE);
                    delete pickedHex;
                    pickedHex = nullptr;
                }
            }

            if (m.timer != 0)
            {
                std::string s = "Timer: " + std::to_string((int)game.getTimerVal());
                DrawText(s.c_str(), screenWidth - 130, 20, 20,
                         getColor(game.getCurrentPlayer()));
            }

            GameOver = game.getRoundLimit() == game.getCurrentRound() - 1;
            std::string plRound;
            if (!GameOver)
            {
                plRound =
                    "PLAYER " + std::to_string(*game.getCurrentPlayer()) +
                    "'S TURN,\t\tROUNDS REMAINING: " +
                    std::to_string(game.getRoundLimit() - game.getCurrentRound() + 1);
                DrawText(plRound.c_str(),
                         (int)screenWidth / 2 - MeasureText(plRound.c_str(), 28) / 2,
                         (int)screenHeight / 12, 28, getColor(game.getCurrentPlayer()));
            }
            else
            {
                int winner = getWinner(&playerScores);
                if (winner)
                {
                    plRound = "GAME OVER! PLAYER " + std::to_string(winner) + " WON!";
                    StopSoundMulti();
                    sounds.play(VICTORY);
                }
                else
                {
                    plRound = "GAME OVER! IT'S A TIE!";
                    StopSoundMulti();
                    sounds.play(DRAW);
                }
                DrawText(plRound.c_str(),
                         (int)screenWidth / 2 - MeasureText(plRound.c_str(), 28) / 2,
                         (int)screenHeight / 12, 28, getColor(&winner));
            }

            if (showOptions)
                drawEscMenu(&o, &sounds);

            drawPlayerScores(&playerScores);

            pool.drawPool(&shouldClear);
            if (pool.getPathFlag() && !GameOver)
            {
                this->setLastClickedId(pool.getPath());
                sounds.play(SELECT);
            }

            if (showHotkeys)
                drawHotkeys();

            else
            {
                std::string text = "Press H to show hotkeys";
                DrawText(text.c_str(), 1250, 800, 16, WHITE);
            }
            if (selected)
                drawSelectedTooltip();

            ImGui::Render();
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        }
        EndDrawing();
    }

    UnloadModel(tower);
    UnloadMusicStream(m_music);
    CloseAudioDevice();
    ImGui_ImplRaylib_Shutdown();
    ImGui_ImplOpenGL3_Shutdown();
}

void App::initImgui(int w, int h)
{
    ImGui_ImplRaylib_ProcessEvent();
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplRaylib_NewFrame();
    ImGui::NewFrame();
}
void App::init()
{
    SetTraceLogLevel(LOG_NONE);
    SetConfigFlags(FLAG_MSAA_4X_HINT);
    raylib::Color textColor(LIGHTGRAY);
}

Camera App::makeCamera()
{
    Camera camera = {0};
    camera.position = (Vector3){-5.0f, 5.0f, 0.0f};
    camera.target = (Vector3){0.0f, 0.0f, 0.0f};
    camera.up = (Vector3){0.0f, 1.0f, 0.0f};
    camera.fovy = 45.0f;
    camera.projection = CAMERA_PERSPECTIVE;

    return camera;
}

void App::setLastClickedId(hexID h) { lastClickedID = h; }

Color App::getColor(int *p)
{
    switch (*p)
    {
    case (0):
        return {0, 0, 0, 255};
    case (1):
        return {40, 40, 200, 255};
    case (2):
        return {225, 80, 0, 255};
    case (3):
        return {30, 150, 30, 255};
    case (4):
        return {191, 85, 200, 255};
    default:
    {
    };
    }
    throw "Wrong player index passed to getColor()";
}

int App::getWinner(std::vector<int> *scores)
{
    int currWinner = 0;
    int currMax = -10000;
    for (int i = 0; i < scores->size(); i++)
    {
        if ((*scores)[i] > currMax)
        {
            currWinner = i + 1;
            currMax = (*scores)[i];
        }
        else if ((*scores)[i] == currMax)
        {
            currWinner = 0;
        }
    }
    return (currWinner);
}