#include "../include/options.hpp"

static int width = 380;

Options::Options(){
}

void Options::drawOptions(){

    Menu m;
    ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.138f, 0.5f, 0.5f, 0.65));
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.138f, 0.7f, 0.5f, 0.65));
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0.138f, 0.9f, 0.5f, 0.65));

    if(ImGui::Button("Music [On / Off]",  ImVec2(width, 50))){
        m_playPauseMusic = !m_playPauseMusic;
    }
    ImGui::Spacing();

    if(ImGui::Button("Fullscreen [On / Off]",  ImVec2(width, 50))){
        ToggleFullscreen();
    };
    ImGui::Spacing();

    if(ImGui::Button("Back", ImVec2(width, 50))){
        setOptionsFlag(false);
        m.setInMainMenuFlag(true);
    }        
    ImGui::PopStyleColor(3);
    

    
}