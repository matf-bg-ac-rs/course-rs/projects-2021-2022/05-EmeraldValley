#include "app.hpp"
#include <string>

void App::DrawHexes(hexMap *map, bool decor, int *currentPlayer,
                    Camera *camera) {
  float hexSpacing = getHexSpacing();
  for (auto &h : *map) {
    DrawHex(h.second, decor, currentPlayer, camera);
  }
}

void App::DrawHex(Hex *h, bool decor, int *currentPlayer, Camera *camera) {
  float hexSpacing = getHexSpacing();
  Vector3 position = {0.0f, 0.0f, 0.0f};
  std::pair<int, int> coords = h->getCoords();
  h->updatePositionProgress();
  float q, r;
  if (!h->isVacant() && animateHexMovement) {
    q = h->getQProgress();
    r = h->getRProgress();
  } else {
    q = coords.first;
    r = coords.second;
  }
  position.x += hexSpacing * (2.0 * q + r);
  position.z += hexSpacing * (sqrt(3) * r);

  float timeDelta = GetFrameTime();

  float rot = h->getRotation();
  float rotP = h->getRotationProgress();

  bool lower = rotP <= rot;
  rotP += timeDelta * 300;
  if (rotP > 360.0) rotP = 0.0;
  if (rotP > rot && lower) rotP = rot;

  (*h->getModel()).transform = MatrixRotateY(DEG2RAD * rotP);

  if (rotP == 360.0) rotP = 0;
  h->setRotationProgress(rotP);

  if (h == pickedHex && !placeable) {
    setLights(INVALID);
    DrawModel(*h->getModel(), position, 2.0f, WHITE);
  } else if (h == pickedHex && placeable) {
    setLights(REG);
    DrawModel(*h->getModel(), position, 2.0f, WHITE);
  } else if (h->isVacant() && !decor) {
    DrawModel(*h->getModel(), position, 1.66f, WHITE);
  } else if (h->isVacant() && decor) {
    DrawModel(*h->getModel(), position, 1.0f, WHITE);
  } else if (!h->isVacant() && decor) {
    setLights(REG);
    DrawModel(*h->getModel(), position, 2.0f, WHITE);
  } else if (App::selected && (coords == App::selectedHex)) {
    setLights(SEL);
    DrawModel(*h->getModel(), position, 2.0f, WHITE);
  } else {
    if (showPlayerColors)
      setLights((lightType)(h->getOwner() + 2));
    else
      setLights(REG);
    DrawModel(*h->getModel(), position, 2.0f, WHITE);
  }
}

void App::drawEscMenu(Options *o, soundPlayer *sounds) {
  ImGuiWindowFlags window_flags =
      ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoBackground |
      ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize |
      ImGuiWindowFlags_NoTitleBar;
  ImGui::Begin("MainMenu", 0, window_flags);
  ImGui::SetNextWindowPos(
      ImVec2(GetScreenWidth() / 2 - 380 / 2, GetScreenHeight() / 2 - 300));
  ImGui::SetNextWindowSize(ImVec2(400, 350));
  ImGui::PushStyleColor(ImGuiCol_Button,
                        (ImVec4)ImColor::HSV(0.138f, 0.5f, 0.5f, 0.65));
  ImGui::PushStyleColor(ImGuiCol_ButtonHovered,
                        (ImVec4)ImColor::HSV(0.138f, 0.7f, 0.5f, 0.65));
  ImGui::PushStyleColor(ImGuiCol_ButtonActive,
                        (ImVec4)ImColor::HSV(0.138f, 0.9f, 0.5f, 0.65));

  if (ImGui::Button("Music [On / Off]", ImVec2(380, 100))) {
    o->m_playPauseMusic = !o->m_playPauseMusic;
    sounds->play(CLICK);
  }
  ImGui::Spacing();

  if (ImGui::Button("Fullscreen [On / Off]", ImVec2(380, 100))) {
    ToggleFullscreen();
    sounds->play(CLICK);
  };
  ImGui::Spacing();

  if (ImGui::Button("Quit Game", ImVec2(380, 100))) {
    gameShouldClose = true;
  }

  ImGui::PopStyleColor(3);
  ImGui::End();
}

void App::drawPlayerScores(std::vector<int> *scores) {
  ImGuiWindowFlags window_flags =
      ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoBackground |
      ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize |
      ImGuiWindowFlags_NoTitleBar;

  ImGui::SetNextWindowPos(ImVec2(0, 0));
  ImGui::SetNextWindowSize(ImVec2(400, 800));

  ImGui::Begin("Scores", 0, window_flags);

  window_flags &= !ImGuiWindowFlags_NoBackground;

  for (int i = 0; i < scores->size(); i++) {
    std::string s =
        "Player " + std::to_string(i + 1) + ": " + std::to_string((*scores)[i]);
    switch (i) {
      case (0): {
        ImGui::PushStyleColor(ImGuiCol_Button,
                              (ImVec4)ImColor::HSV(0.66f, 0.6f, 0.98f, 0.8));
        break;
      }
      case (1): {
        ImGui::PushStyleColor(ImGuiCol_Button,
                              (ImVec4)ImColor::HSV(0.1f, 0.75f, 1.0f, 0.8));
        break;
      }
      case (2): {
        ImGui::PushStyleColor(ImGuiCol_Button,
                              (ImVec4)ImColor::HSV(0.33f, 0.6f, 0.98f, 0.8));
        break;
      }
      case (3): {
        ImGui::PushStyleColor(ImGuiCol_Button,
                              (ImVec4)ImColor::HSV(0.82f, 0.45f, 0.78f, 0.8));
        break;
      }
      default:
        throw "Wrong number of players passed to the scoreboard generator!";
    }
    ImGui::Button(s.c_str(), ImVec2(300, 40));
    ImGui::Spacing();
    ImGui::PopStyleColor(1);
  }
  ImGui::End();
};

void App::drawHotkeys() {
  ImGuiWindowFlags window_flags =
      ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoMove |
      ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar;

  ImGui::SetNextWindowPos(ImVec2(1180, 780));
  ImGui::SetNextWindowSize(ImVec2(300, 85));

  ImGui::PushStyleColor(ImGuiCol_WindowBg,
                        (ImVec4)ImColor::HSV(0.138f, 0.7f, 0.5f, 0.65f));
  ImGui::Begin("Hotkeys", 0, window_flags);

  ImGui::Text(
      "H to show or hide hotkeys.\nJ to toggle Hex scores.\nK to "
      "disable coloring Hexes by players.\nO to disable Hex movement "
      "animations.\nSpace to end turn without placing a hex.");

  ImGui::End();
  ImGui::PopStyleColor(1);
};

void App::drawSelectedTooltip() {
  ImGui::BeginTooltip();
  if (selectedHexID != hexID::CASTLE)
    ImGui::Text(getToolTipFromID(selectedHexID).c_str());
  ImGui::EndTooltip();
}
