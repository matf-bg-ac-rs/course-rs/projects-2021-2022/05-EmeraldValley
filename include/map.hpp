#ifndef _MAP_H_
#define _MAP_H_

#include <raylib-cpp.hpp>
#include <vector>
#include <map>
#include "hex.hpp"

typedef std::map<std::pair<int, int>, Hex*> hexMap;

class Map {
public:
    Map();
    ~Map();
    void Insert(std::pair<int, int> &coord, Hex *h);
    // Da li postoji hex sa datim koordinatama.
    Hex* Find(std::pair<int, int> clickedHex);
    hexMap* getMap();
    std::vector<HexBasic*> getAdjacentHexes(int q, int r, std::vector<HexBasic*> *adjHexVec);
    bool checkPlaceable(HexBasic* h, int radius);
    void checkEdges(HexBasic* newHex, HexBasic* adjHex, bool* conditionMet, bool* restrictionBroken, int i);
    void applyBonuses(HexBasic* orig, HexBasic* adj, int i, std::vector<int>*scores);
    std::pair<int, int> hexDirections[6] = {
        { 1, -1}, { 1, 0}, { 0,  1},
        {-1,  1}, {-1, 0}, { 0, -1}
    };

    // Generisi mapu u zavisnosti od broja igraca
    // Stavljena ovde radi lakseg debagovanja
    int generateMap(int numOfPlayers, bool real);
private:
    std::vector<std::pair<int, int>> coords;
    hexMap m_hexMap;

};

void writePlacementMessage(hexID id);



#endif // _MAP_H_
