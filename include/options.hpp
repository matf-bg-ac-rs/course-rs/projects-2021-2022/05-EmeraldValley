#pragma once

#include <raylib-cpp.hpp>
#include "../include/imgui/imgui.h"
#include "../include/imgui/imgui_impl_opengl3.h"
#include "../include/imgui/imgui_impl_raylib.h"
#include "../include/menu.hpp"



class Options{
public:
    Options();
    void drawOptions();
    void inline setOptionsFlag(bool b) { m_optionsWindowFlag = b;}
    bool inline getOptionsFlag() { return m_optionsWindowFlag;}
    inline static bool m_optionsWindowFlag = false;
    inline static bool m_playPauseMusic = true;

private:
	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar;

};
