#ifndef _GAME_HPP_
#define _GAME_HPP_

#include <vector>

#include "hex.hpp"
#include "player.hpp"
#include "map.hpp"

class Game {
public:
    Game();
    inline void setTimerVal(float t) {timerDur = t; resetTimer();}
    inline void resetTimer() {timerVal = timerDur;}
    inline float getTimerVal() {return timerVal;}
    bool updateTimer();
    inline void setNumOfPlayers(int n) {numOfPlayers = n;}
    inline int* getCurrentPlayer() {return &currentPlayer;}
    inline int getCurrentRound() {return currentRound;}
    inline int getRoundLimit() {return roundLimit;}
    void setNextPlayer();
    void endTurn();
private:
    float timerDur;
    float timerVal;
    int numOfPlayers;
    int currentPlayer = 1;
    int currentRound = 1;
    int roundLimit = 30;
};



#endif // GAME_H
