#ifndef _SOUNDS_HPP_
#define _SOUNDS_HPP_

#include "raylib-cpp.hpp"
#include "enums.hpp"

class soundPlayer{
public:
    soundPlayer();
    ~soundPlayer();
    void play(SoundID s);
private:
    Sound hexSelect;
    Sound hexPlaced;
    Sound hexError;
    Sound hexRotate;
    Sound uiClick;
    Sound exitPrompt;
    Sound victory;
    Sound draw;
    Sound timerExpired;
};

#endif