#pragma once

#include <raylib-cpp.hpp>
#include "../include/imgui/imgui.h"
#include "../include/imgui/imgui_impl_opengl3.h"
#include "../include/imgui/imgui_impl_raylib.h"
#include "../include/menu.hpp"

#include <string>
#include <vector>
#include <iostream>

class Setup{
public:
    Setup();
    void drawSetup();
    int getNumPlayers();
    int getSetupTimer();
    void inline setSetupFlag(bool b) { m_setupWindowFlag = b;}
    bool inline getSetupFlag() { return m_setupWindowFlag;}
    inline static bool m_setupWindowFlag = false;

    bool isGameReady = false;
private:
    int m_numOfPlayers = 0;
    std::vector<int> m_timerOptions = {0, 20, 40};
    int m_timerSetup = 20;
	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar;

};
