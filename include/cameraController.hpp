#ifndef _CAMERA_CONTROLLER_HPP_
#define _CAMERA_CONTROLLER_HPP_

#include <raylib-cpp.hpp>

class cameraController
{
public:
    cameraController(Camera* camera){
        m_camera = camera;
    }

    ~cameraController();

    void WASDpressed();
    void mWheelMoved(float wheel);
    void MMBpressed();

private:
    Camera* m_camera;
};


#endif