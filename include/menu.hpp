#pragma once

#include "../include/options.hpp"
#include "../include/newGameSetup.hpp"

#include "raylib.h"
#include "../include/imgui/imgui.h"
#include "../include/imgui/imgui_impl_opengl3.h"
#include "../include/imgui/imgui_impl_raylib.h"
#include <iostream>


class Menu{
public:
	Menu();
	void drawMainMenu();
	void initMainMenu();
	void inline setMainMenuFlag(bool b) { m_setupMainMenuFlag = b;}
    bool inline getMainMenuFlag() { return m_setupMainMenuFlag;}
    inline static bool m_setupMainMenuFlag = true;
	void inline setInMainMenuFlag(bool b) { m_setupInMainMenuFlag = b;}
    bool inline getInMainMenuFlag() { return m_setupInMainMenuFlag;}
    inline static bool m_setupInMainMenuFlag = true;
    inline bool getShouldClose() {return m_gameShouldClose;}

    bool IsGameReady() const;
    int numOfPlayers;
    int timer;
private:
	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar;
	int width = 380;
    bool m_gameShouldClose = false;

};
