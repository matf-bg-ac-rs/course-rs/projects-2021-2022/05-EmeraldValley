#ifndef HEX_HPP
#define HEX_HPP

#include "./player.hpp"
#include <tuple>
#include <string>
#include <raylib-cpp.hpp>
#include <vector>
#include "enums.hpp"


std::vector<hexID> generateEdgesFromType(hexID id);

class Hex
{
public:
    Hex();
    Hex(int q, int r, hexID type);
    virtual ~Hex();

    Model* getModel();

    virtual inline void incrementRotation(){}
    virtual inline void setRotation(int rot){}
    virtual inline void setRotationProgress(float f){}
    virtual  void updatePositionProgress(){}
    inline std::pair<int, int> getCoords() const        {return std::make_pair(m_q, m_r);}
    virtual inline float getRotation() const            {return 0;}
    virtual inline float getRotationProgress() const    {return 0;}
    virtual inline float getQProgress() const           {return 0;}
    virtual inline float getRProgress() const           {return 0;}
    virtual inline bool isVacant()                      {return true;}
    virtual inline int getOwner() const                 {return 0;}
    inline hexID getID()                                {return id;}

protected:
    int m_q;
    int m_r;
    int m_s;
    Model model;
    hexID id;

    friend bool operator==(const Hex& a, const Hex& b);
    friend bool operator!=(const Hex& a, const Hex& b);
};

class HexBasic : public Hex
{
public:
    HexBasic();
    HexBasic(int q, int r, hexID type, float rotation, unsigned int owner = 0);
    virtual ~HexBasic() = default;

    inline void setOwner(int i)                         {m_owner = i;}
    inline int getOwner() const  override               {return m_owner;}
    inline float getRotation() const override           {return m_rotation;}
    inline float getRotationProgress() const override   {return m_rotation_progress;}
    inline float getQProgress() const override          {return m_q_progress;}
    inline float getRProgress() const override          {return m_r_progress;}

    void setCoords(int q, int r);
    void incrementRotation() override;
    void setRotation(int rot) override;
    void setRotationProgress(float f) override;
    void updatePositionProgress() override;
    inline bool isVacant() override                     {return false;}
    virtual inline hexID getEdgeID(unsigned n)          {return id;}
    inline void modifyScore(int i)                      {score += i;}    
    inline int getScore()                               {return score;}               

protected:
    float m_rotation = 0;
    float m_rotation_progress = 0;
    float m_q_progress = 0;
    float m_r_progress = 0;
    unsigned int m_owner = 0;
    int score = 0;
};

class HexModular : public HexBasic
{
public:
    HexModular(int q, int r, hexID type, float rotation, unsigned int owner = 0);
    ~HexModular() = default;

    // bool checkRequirements() override;
    inline hexID getEdgeID(unsigned n) override         {return edges[(n-((int)(m_rotation/60.0))+6)%6];}

private:
    std::vector<hexID> edges;
};

std::ostream & operator<<(std::ostream &out, Hex &h);

bool getModularFromID(hexID id);
std::string getPathFromID(hexID id);
hexID getIDfromImagePath(std::string s);
cat getCategoryFromID(hexID id);

#endif
