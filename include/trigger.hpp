#pragma once

#include "raylib.h"
#include "../include/imgui/imgui.h"
#include "../include/imgui/imgui_impl_opengl3.h"
#include "../include/imgui/imgui_impl_raylib.h"
#include "hex.hpp"

#include <string>

typedef struct{
    Texture2D texture;
    std::string path; 
    
} MyTexture;

class Trigger
{
public:
	Trigger();
	void drawPool(bool* shouldClear);
	void updateButton(unsigned int button);
	int getButton();
	hexID getPath();
	void updatePath(std::string path);
	bool inline getPathFlag() { return pathFlag;}
	std::string inline getStaticPath() { return staticPath;}
private:
	void inline setPathFlag(bool b) { pathFlag = b;}
	int m_button = 0;
	std::string m_path;
	std::string staticPath;
	bool pathFlag = false;
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoScrollbar;

};