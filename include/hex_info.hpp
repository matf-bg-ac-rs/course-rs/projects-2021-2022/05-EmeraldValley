#pragma once
#include "raylib.h"
#include "../include/imgui/imgui.h"
#include "../include/imgui/imgui_impl_opengl3.h"
#include "../include/imgui/imgui_impl_raylib.h"

#include "../include/app.hpp"
#include "../include/hex.hpp"

#include <string>

typedef struct{
   std::pair<int, int> selectedHex;
    
} MyHexInfo;

class HexInfo
{
public:
	HexInfo();
	void drawHexInfo();
private:
};
